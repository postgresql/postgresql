# Catalan translation of postgresql-17's debconf messages
# Copyright © 2024 Free Software Foundation, Inc.
# This file is distributed under the same license as the postgresql-17 package.
# poc senderi <pocsenderi@protonmail.com>, 2024.
#
msgid ""
msgstr ""
"Project-Id-Version: postgresql-11\n"
"Report-Msgid-Bugs-To: postgresql-11@packages.debian.org\n"
"POT-Creation-Date: 2019-01-09 15:22+0100\n"
"PO-Revision-Date: 2024-11-11 22:07+0100\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Last-Translator: poc senderi <pocsenderi@protonmail.com>\n"
"Language-Team: Catalan <debian-l10n-catalan@lists.debian.org>\n"
"X-Generator: Poedit 2.4.2\n"

#. Type: boolean
#. Description
#: ../postgresql-11.templates:1001
msgid "Remove PostgreSQL directories when package is purged?"
msgstr ""
"Voleu esborrar els directoris del PostgreSQL quan el paquet sigui purgat?"

#. Type: boolean
#. Description
#: ../postgresql-11.templates:1001
msgid ""
"Removing the PostgreSQL server package will leave existing database clusters "
"intact, i.e. their configuration, data, and log directories will not be "
"removed. On purging the package, the directories can optionally be removed."
msgstr ""
"A l'esborrar el paquet del servidor PostgreSQL romandran les bases de dades "
"intactes, això és, la configuració, les dades i els directoris de registres "
"no seran esborrats. Al purgar el paquet, aquests directoris poden ser "
"opcionalment també esborrats."
